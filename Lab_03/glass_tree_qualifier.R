library(mlbench)

library(tree)
library(maptree)

data(Glass)

Glass.tree <- tree(Type ~., Glass)
draw.tree(Glass.tree, cex = 0.7)

example <- data.frame(RI =  1.516,
                      Na = 11.700,
                      Mg =  1.010,
                      Al =  1.190,
                      Si = 72.590,
                      K  =  0.430,
                      Ca = 11.440,
                      Ba =  0.020, 
                      Fe =  0.100)

print(predict(Glass.tree, newdata = example, type = "class"))