library(kknn)

data(glass)
n <- dim(glass)[1]  # size of data

glass <- glass[, -1]
glass <- glass[order(runif(n)),] # shuffle data

part <- 0.75
sep  <- as.integer(n*part)

trainData <- glass[1:sep,    ]
testData  <- glass[(sep+1):n,]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

rightwrong <- function(kknnmodel) {
  fit <- fitted(kknnmodel)
  t   <- table(testData$Type, fit)
  right <- sum(diag(t)) / dim(testData)[1] * 100
  wrong <- 100.0 - right
  
  return(c(right, wrong))
}

testKer  <- "rectangular"
testDist <- 1
testK    <- 16

exceptionsDf <- data.frame(rightwrong(kknn(Type~., trainData, testData, kernel   = testKer,
                                           distance = testDist,
                                           k        = testK)),
                           #-----------------------------------------------------------------#
                           rightwrong(kknn(Type~., trainData, testData, kernel   = testKer)),
                           rightwrong(kknn(Type~., trainData, testData, distance = testDist)),
                           rightwrong(kknn(Type~., trainData, testData, k        = testK)),
                           #-----------------------------------------------------------------#
                           rightwrong(kknn(Type~., trainData, testData, kernel   = testKer,
                                           distance = testDist)),
                           #                                            ---------------------#
                           rightwrong(kknn(Type~., trainData, testData, kernel   = testKer,
                                           k        = testK)),
                           #                                            ---------------------#
                           rightwrong(kknn(Type~., trainData, testData, distance = testDist,
                                           k = testK)))
rownames(exceptionsDf) <- c("Right", "Wrong")
colnames(exceptionsDf) <- c("Kr/D/k",
                            "Kr", "D", "k",
                            "Kr/D", "Kr/k", "D/k")
print(exceptionsDf)

glassExample <- data.frame(RI = 1.516, Na = 11.7, Mg = 1.01, Al = 1.19, Si = 72.59, K = 0.43, Ca = 11.44, Ba = 0.02, Fe = 0.1)

print(fitted(kknn(Type~., trainData, glassExample, distance = 1)))

